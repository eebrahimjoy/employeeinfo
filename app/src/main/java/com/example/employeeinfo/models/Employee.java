package com.example.employeeinfo.models;

public class Employee {
    private String Id;
    private String name;
    private String designation;
    private String salary;
    private String joiningDate;
    private String address;
    private String latitude;
    private String longitude;


    public Employee(){

    }

    public Employee(String id, String name, String designation, String salary, String joiningDate, String address, String latitude, String longitude) {
        Id = id;
        this.name = name;
        this.designation = designation;
        this.salary = salary;
        this.joiningDate = joiningDate;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(String joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
