package com.example.employeeinfo.models;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.employeeinfo.R;

import java.util.ArrayList;
import java.util.List;


public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.EmployeeViewHolder> implements Filterable {

    private ArrayList<Employee> employees;
    private ArrayList<Employee> employeesList;
    private Context context;
    private double salary;

    public EmployeeAdapter(ArrayList<Employee> employees, Context context) {
        this.employees = employees;
        this.context = context;
        this.employeesList = employees;
    }


    @Override
    public EmployeeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.model_employee_item,parent,false);
        return new EmployeeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(EmployeeViewHolder holder, int position) {
        holder.employeeNameValue.setText(employees.get(position).getName());
        holder.employeeDesignationValue.setText(employees.get(position).getDesignation());
        holder.employeeSalaryValue.setText(employees.get(position).getSalary());
    }

    @Override
    public int getItemCount() {
        return employees.size();
    }

    @Override
    public Filter getFilter() {
        return employeeFilter;
    }
    private Filter employeeFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<Employee> filterEmployee = new ArrayList<>();
            if (charSequence == null || charSequence.length() == 0){
                filterEmployee.addAll(employeesList);
            }
            else {
                String filterPattern = charSequence.toString().toLowerCase().trim();

                for (Employee employeeT : employeesList){
                    if (employeeT.getName().toLowerCase().contains(filterPattern)){
                        filterEmployee.add(employeeT);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filterEmployee;
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            employees.clear();
            employees.addAll((List) filterResults.values);
            notifyDataSetChanged();

        }
    };

    public class EmployeeViewHolder extends RecyclerView.ViewHolder{


        TextView employeeNameValue;
        TextView employeeDesignationValue;
        TextView employeeSalaryValue;

        public EmployeeViewHolder(View itemView) {
            super(itemView);
            employeeNameValue = itemView.findViewById(R.id.employeeNameListTV);
            employeeDesignationValue = itemView.findViewById(R.id.designationTv);
            employeeSalaryValue = itemView.findViewById(R.id.salaryListTV);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
}
