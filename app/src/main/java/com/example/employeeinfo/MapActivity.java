package com.example.employeeinfo;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    Marker searchedMarker = null;
    private GoogleMap map;
    private GoogleMap.OnCameraIdleListener onCameraIdleListener;
    private GoogleMapOptions options;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private static final int INTENT_FOR_RESULT = 1;
    private double latMap, lonMap, resultLat, resultLon;
    int somthing;
    LatLng searchLatLng;
    Button inputSearchBtn,iSearchBtn;
    String userSearchAddress;
    String shopCatchValue;

    String employeeNameC, salaryC, joiningDateC,shopTypeC,imageC;
    int designationC;

    private Geocoder geocoder;
    private String addressLine;
    private FloatingActionButton currentLocation,locationFab;
    ImageView marker;
    LinearLayout linearInvisible;
    String locality;
    Double latitudedrag;
    Double longitudedrag;
    int flagForActivityChange;



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == INTENT_FOR_RESULT) {
            if (resultCode == Activity.RESULT_OK) {
                somthing = data.getIntExtra("check", 0);

                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.custom_alert_box, null);
                final TextView showBtn = alertLayout.findViewById(R.id.showBTN);
                final TextView changeLocation = alertLayout.findViewById(R.id.changeLocationTV);
                final TextView selectLocation = alertLayout.findViewById(R.id.selectLocationTV);

                resultLat = data.getDoubleExtra("latitude", 0);
                resultLon = data.getDoubleExtra("longitude", 0);
                searchLatLng = new LatLng(resultLat, resultLon);
                userSearchAddress = data.getStringExtra("lineAddress");
                showBtn.setText(userSearchAddress);
                changeLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent =new Intent(MapActivity.this,SearchActivity.class);
                        startActivityForResult(intent,INTENT_FOR_RESULT);
                    }
                });

                selectLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent returnIntent = new Intent(MapActivity.this,RegistrationActivity.class);
                        returnIntent.putExtra("check1", 1);
                        returnIntent.putExtra("latitude", resultLat);
                        returnIntent.putExtra("longitude", resultLon);
                        returnIntent.putExtra("address", userSearchAddress);

                        returnIntent.putExtra("employeeNameD", employeeNameC);
                        returnIntent.putExtra("salaryD", salaryC);
                        returnIntent.putExtra("designationD", designationC);
                        returnIntent.putExtra("joiningDateD", joiningDateC);

                        startActivity(returnIntent);
                        finish();
                    }
                });
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setView(alertLayout);
                AlertDialog dialog = alert.create();
                dialog.show();
                if (searchedMarker != null) {
                    searchedMarker.remove();
                }
                searchedMarker = map.addMarker(new MarkerOptions().position(searchLatLng)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
                map.animateCamera(CameraUpdateFactory.newLatLng(searchLatLng));








            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        inputSearchBtn = findViewById(R.id.inputSearchId);
        currentLocation = findViewById(R.id.currentLocationFAB);
        iSearchBtn=findViewById(R.id.iSearchId);
        locationFab=findViewById(R.id.locationFAB);
        marker=findViewById(R.id.marker);
        linearInvisible=findViewById(R.id.linearInvisible);
        getMapReady();
        getLastLocation();


        locationFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent returnIntent = new Intent(MapActivity.this,RegistrationActivity.class);
                returnIntent.putExtra("latitude", latitudedrag);
                returnIntent.putExtra("check1", 1);
                returnIntent.putExtra("longitude", longitudedrag);
                returnIntent.putExtra("address", locality);
                returnIntent.putExtra("employeeNameD", employeeNameC);
                returnIntent.putExtra("salaryD", salaryC);
                returnIntent.putExtra("designationD", designationC);
                returnIntent.putExtra("joiningDateD", joiningDateC);
                startActivity(returnIntent);
                finish();

            }
        });

        Bundle getExtraMapE = getIntent().getExtras();
        if (getExtraMapE != null) {
            employeeNameC = getExtraMapE.getString("Name");
            salaryC = getExtraMapE.getString("Salary");
            designationC = getExtraMapE.getInt("Designation");
            joiningDateC = getExtraMapE.getString("Date");
        }



        currentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLastLocation();

            }
        });


        iSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapActivity.this,SearchActivity.class);
                startActivityForResult(intent, INTENT_FOR_RESULT);
            }
        });
    }

    private void getMapReady() {
        options = new GoogleMapOptions().rotateGesturesEnabled(true).mapType(GoogleMap.MAP_TYPE_TERRAIN);
        SupportMapFragment mapFragment = SupportMapFragment.newInstance(options);
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                .beginTransaction().replace(R.id.mapContainerId, mapFragment);
        fragmentTransaction.commit();
        mapFragment.getMapAsync(this);
        configureCameraIdle();

    }

    private void configureCameraIdle() {
        onCameraIdleListener = new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {

                LatLng latLng = map.getCameraPosition().target;
                Geocoder geocoder = new Geocoder(MapActivity.this);

                try {
                    List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                    if (addressList != null && addressList.size() > 0) {
                        locality = addressList.get(0).getAddressLine(0);
                        latitudedrag = addressList.get(0).getLatitude();
                        longitudedrag=addressList.get(0).getLongitude();
                        if (!locality.isEmpty())
                            inputSearchBtn.setText(locality);


                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        map = googleMap;
        map.setOnCameraIdleListener(onCameraIdleListener);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission
                .ACCESS_FINE_LOCATION) != PackageManager
                .PERMISSION_GRANTED && ActivityCompat
                .checkSelfPermission(this, Manifest.permission
                        .ACCESS_COARSE_LOCATION) != PackageManager
                .PERMISSION_GRANTED) {
            getUserPermission();


           /* map.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
                @Override
                public void onCameraMove() {
                    LatLng target = map.getCameraPosition().target;
                    Toast.makeText(MapActivity.this, ""+target, Toast.LENGTH_SHORT).show();
                }
            });*/
        }


    }


    private void getLastLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            getUserPermission();
        }
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null){
                    latMap = location.getLatitude();
                    lonMap = location.getLongitude();
                    LatLng latLngMap = new LatLng(latMap, lonMap);

                    geocoder = new Geocoder(MapActivity.this, Locale.getDefault());
                    List<Address> addresses;

                    try {
                        addresses = geocoder.getFromLocation(latMap, lonMap, 1);
                        addressLine = addresses.get(0).getAddressLine(0);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLngMap, 10);

                        map.animateCamera(cameraUpdate);
                        map.getUiSettings().setMapToolbarEnabled(false);
                        map.getUiSettings().setMyLocationButtonEnabled(false);
                        if (ActivityCompat.checkSelfPermission(MapActivity.this,
                                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager
                                .PERMISSION_GRANTED && ActivityCompat.
                                checkSelfPermission(MapActivity.this, Manifest.permission
                                        .ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            getUserPermission();
                        }
                        map.setMyLocationEnabled(true);




                }




            }
        });


    }


    private void getUserPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);
        return;
    }
}
