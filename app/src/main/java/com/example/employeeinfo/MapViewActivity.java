package com.example.employeeinfo;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.employeeinfo.models.Employee;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MapViewActivity extends AppCompatActivity implements OnMapReadyCallback {
    Location currentLocation;
    FusedLocationProviderClient fusedLocationProviderClient;
    ArrayList<Employee> employeesMap;

    private static final int R_C = 101;
    GoogleMap eMap;
    double latFromF,lonFromF;
    String nameE;
    DatabaseReference databaseReferenceE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map2);
        employeesMap = new ArrayList<>();
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        fetchLocation();

        databaseReferenceE = FirebaseDatabase.getInstance().getReference("employees");




    }

    private void fetchLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION},R_C);
            return;
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null){
                    currentLocation = location;
                    //Toast.makeText(MapViewActivity.this, ""+currentLocation.getLatitude(), Toast.LENGTH_SHORT).show();
                    SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.google_map);
                    supportMapFragment.getMapAsync(MapViewActivity.this);
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        eMap = googleMap;

        databaseReferenceE.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                for (DataSnapshot employeeDataSnapshot : dataSnapshot.getChildren()) {
                    Employee employeeS = employeeDataSnapshot.getValue(Employee.class);
                    employeesMap.add(employeeS);

                }
                for (int i=0;i<employeesMap.size();i++){
                    latFromF = Double.valueOf(employeesMap.get(i).getLatitude());
                    lonFromF = Double.valueOf(employeesMap.get(i).getLongitude());
                    LatLng latLng = new LatLng(latFromF, lonFromF);
                    String nameE = employeesMap.get(i).getName();

                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng)
                            .icon(BitmapDescriptorFactory
                                    .fromBitmap(createCustomMarker(MapViewActivity.this,nameE)));
                    eMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 8));

                    eMap.setMinZoomPreference(6.0f);
                    eMap.setMaxZoomPreference(17.0f);
                    eMap.addMarker(markerOptions).setTag(employeesMap.get(i));

                }

                eMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        if (marker != null) {

                            Employee employeeT = (Employee) marker.getTag();
                            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(MapViewActivity.this);
                            View view = getLayoutInflater().inflate(R.layout.bottom_sheet_layout_for_outlet_details, null);
                            bottomSheetDialog.setContentView(view);
                            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) view.getParent());
                            bottomSheetBehavior.setPeekHeight(
                                    (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 350, getResources()
                                            .getDisplayMetrics())
                            );
                            bottomSheetDialog.show();



                            TextView salaryAmountT = view.findViewById(R.id.salaryAmountTV);
                            salaryAmountT.setText(employeeT.getSalary());
                            TextView employeeFullNameT = view.findViewById(R.id.employeeFullNameTV);
                            employeeFullNameT.setText(employeeT.getName());
                            TextView employeeAddressT = view.findViewById(R.id.employeeAddressTV);
                            employeeAddressT.setText(employeeT.getAddress());
                            TextView joiningDateT = view.findViewById(R.id.joiningDateTV);
                            joiningDateT.setText(employeeT.getJoiningDate());
                            TextView designation = view.findViewById(R.id.employeeDesignationTV);
                            designation.setText(employeeT.getDesignation());
                        }

                        return false;
                    }
                });


            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case R_C:
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    fetchLocation();
                }
                break;
        }
    }
    public static Bitmap createCustomMarker(Context context, String name) {

        View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);

        TextView discountAmount = marker.findViewById(R.id.nameTV);
        discountAmount.setText(name);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        marker.setLayoutParams(new ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT));
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        marker.draw(canvas);

        return bitmap;
    }
}
