package com.example.employeeinfo;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.employeeinfo.models.Employee;
import com.example.employeeinfo.models.EmployeeAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class EmployeeInfoActivity extends AppCompatActivity {
    FloatingActionButton floatingActionForEmployeeReg;
    private RecyclerView movieRV;
    private EmployeeAdapter employeeAdapter;
    private Employee employeN;
    private DatabaseReference databaseReferenceEm;
   private ArrayList<Employee> employeesL;
   private double sum = 0.0;
   private double salary;
    private TextView totalSalary;

    TextView searchMenuIcon;
    TextView employeeClicked;
    FloatingActionButton mapView;
    SearchView searchView;

    double lat,lon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_info);
        movieRV = findViewById(R.id.movieRV);
        floatingActionForEmployeeReg = findViewById(R.id.addDEmployeeFAB);
        totalSalary = findViewById(R.id.totalSalaryTV);
        searchMenuIcon = findViewById(R.id.searchMenuIconTV);
        employeeClicked = findViewById(R.id.employeeListTV);
        searchView = findViewById(R.id.searchEmployeeSV);
        mapView = findViewById(R.id.mapFAB);

        employeN = new Employee();
        employeesL = new ArrayList<>();
        databaseReferenceEm = FirebaseDatabase.getInstance().getReference("employees");
        searchMenuIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                employeeClicked.setVisibility(View.INVISIBLE);
                searchView.setVisibility(View.VISIBLE);
                searchMenuIcon.setVisibility(View.INVISIBLE);
                mapView.setVisibility(View.INVISIBLE);
            }
        });

        floatingActionForEmployeeReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(EmployeeInfoActivity.this, RegistrationActivity.class);
                startActivity(i);
                finish();
            }
        });

        mapView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             Intent i = new Intent(EmployeeInfoActivity.this, MapViewActivity.class);
             startActivity(i);
             finish();

            }
        });

        databaseReferenceEm.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                for (DataSnapshot employeeDataSnapshot : dataSnapshot.getChildren()) {
                    Employee employeeS = employeeDataSnapshot.getValue(Employee.class);
                    employeesL.add(employeeS);
                    salary = Double.valueOf(employeeS.getSalary());
                    sum = sum + salary;
                }
                totalSalary.setText(String.valueOf(sum));
                employeeAdapter = new EmployeeAdapter(employeesL, EmployeeInfoActivity.this);
                LinearLayoutManager llm = new LinearLayoutManager(EmployeeInfoActivity.this);
                llm.setOrientation(LinearLayoutManager.HORIZONTAL);

                GridLayoutManager glm = new GridLayoutManager(EmployeeInfoActivity.this,2);
                StaggeredGridLayoutManager sglm = new StaggeredGridLayoutManager(1,StaggeredGridLayoutManager.VERTICAL);
                movieRV.setLayoutManager(sglm);
                movieRV.setAdapter(employeeAdapter);

                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        if (newText.isEmpty()) {
                            employeesL.clear();
                            employeeAdapter.notifyDataSetChanged();
                        }
                        employeeAdapter.getFilter().filter(newText);
                        return true;
                    }
                });

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });







    }
}
