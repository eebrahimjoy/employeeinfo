package com.example.employeeinfo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.employeeinfo.models.Employee;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;

public class RegistrationActivity extends AppCompatActivity {
    private static final String message = " Your data is saved successfully";


    EditText employeeName,employeeSalary;
    Spinner employeeDesignation;
    TextView employeeAddress,employeeJoiningDate;
    Button employeeReg;
    String date;

    double searchLat,searchLon;
    String searchAddress;
    int employeeDesignationM,employeeDesignationN;
    String employeeNameM,employeeSalaryM,employeeJoiningDateM;
    String employeeNameN,employeeSalaryN,employeeJoiningDateN;
    private DatePickerDialog.OnDateSetListener dateSetListenerTo;

    DatabaseReference databaseEmployee;
    Snackbar snackbar;
    TextView response;
    private Button placePicker;
    int position;
    String employeeDesignationValue;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode==KeyEvent.KEYCODE_BACK){
            Intent intent = new Intent(RegistrationActivity.this,EmployeeInfoActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        databaseEmployee = FirebaseDatabase.getInstance().getReference("employees");

        employeeName = findViewById(R.id.employeeNameET);
        employeeSalary = findViewById(R.id.salaryAmountET);
        employeeDesignation = findViewById(R.id.designationSP);
        employeeAddress = findViewById(R.id.employeeAddressET);
        employeeJoiningDate = findViewById(R.id.joiningDateTV);
        employeeReg = findViewById(R.id.submitBtnReg);
        response = findViewById(R.id.responseRegTV);
        placePicker = findViewById(R.id.placepickerBTN);

        employeeDesignation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //position = employeeDesignation.getFirstVisiblePosition();
                adapterView.getItemAtPosition(i);
                position = employeeDesignation.getSelectedItemPosition()+1;

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Bundle getExtraShop = getIntent().getExtras();
        if (getExtraShop != null) {
            searchLat = getExtraShop.getDouble("latitude");
            searchLon = getExtraShop.getDouble("longitude");
            searchAddress = getExtraShop.getString("address");
            employeeNameN = getExtraShop.getString("employeeNameD");
            employeeDesignationN = getExtraShop.getInt("designationD");
            employeeJoiningDateN = getExtraShop.getString("joiningDateD");
            employeeSalaryN = getExtraShop.getString("salaryD");
            placePicker.setVisibility(View.GONE);

            employeeAddress.setText(searchAddress);
            employeeSalary.setText(employeeSalaryN);
            employeeName.setText(employeeNameN);
            employeeJoiningDate.setText(employeeJoiningDateN);
            employeeDesignation.setSelection(employeeDesignationN-1);
            Toast.makeText(this, ""+employeeDesignationN, Toast.LENGTH_SHORT).show();
        }


        placePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                employeeNameM = employeeName.getText().toString();
                employeeJoiningDateM = employeeJoiningDate.getText().toString();
                employeeSalaryM = employeeSalary.getText().toString();

                Intent mapIntent = new Intent(RegistrationActivity.this,MapActivity.class);
                mapIntent.putExtra("Name",employeeNameM);
                mapIntent.putExtra("Salary",employeeSalaryM);
                mapIntent.putExtra("Designation",position);
                mapIntent.putExtra("Date",employeeJoiningDateM);
                startActivity(mapIntent);
            }
        });

        employeeReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addEmployee();
            }
        });

        employeeJoiningDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendarTo = Calendar.getInstance();
                int yearTo = calendarTo.get(Calendar.YEAR);
                int monthTo = calendarTo.get(Calendar.MONTH);
                int dayTo = calendarTo.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        RegistrationActivity.this, AlertDialog.THEME_HOLO_LIGHT, dateSetListenerTo, yearTo, monthTo, dayTo);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });
        dateSetListenerTo = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                month = month + 1;
                date = day + "/" + month + "/" + year;
                employeeJoiningDate.setText(date);


            }
        };

    }


    private void addEmployee(){

        employeeDesignationValue = employeeDesignation.getSelectedItem().toString() ;
        String employeeNameValue = employeeName.getText().toString();
        String employeeSalaryValue = employeeSalary.getText().toString();

        if (!TextUtils.isEmpty(employeeNameValue) && !TextUtils.isEmpty(employeeSalaryValue)
        &&!TextUtils.isEmpty(employeeJoiningDateN) && !searchAddress.isEmpty()){

            if (employeeNameValue.length()<=2 && employeeSalaryValue.length()<4 || searchAddress.isEmpty() || employeeDesignationN == 1) {
                if (employeeNameValue.length()<=2){
                    Toast.makeText(this, "Name must be at least 3 character long", Toast.LENGTH_SHORT).show();
                }
                if (employeeSalaryValue.length()<4){
                    Toast.makeText(this, "Salary must be at least 4 digit", Toast.LENGTH_SHORT).show();
                }
                if (searchAddress.isEmpty()){
                    Toast.makeText(this, "Please put an address", Toast.LENGTH_SHORT).show();

                }
                if (employeeDesignationN ==1)
                {
                    Toast.makeText(this, "Select a designation", Toast.LENGTH_SHORT).show();
                }
                if ( employeeJoiningDateN.isEmpty())
                {
                    Toast.makeText(this, "Select a date", Toast.LENGTH_SHORT).show();
                }
                 }
            else {
                String databaseId = databaseEmployee.push().getKey();

                Employee employee = new Employee(databaseId,employeeNameValue,
                        employeeDesignationValue,employeeSalaryValue,employeeJoiningDateN,searchAddress,String.valueOf(searchLat),String.valueOf(searchLon));
                databaseEmployee.child(databaseId).setValue(employee);

                showResponse();

            }


        }
        else {
            Toast.makeText(this, "Please fill-up all the filled", Toast.LENGTH_SHORT).show();
        }

    }
    private void showResponse() {
        snackbar = Snackbar.make(response, "" + message, Snackbar.LENGTH_INDEFINITE)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent returnIntent = new Intent(RegistrationActivity.this,EmployeeInfoActivity.class);
                        startActivity(returnIntent);
                        finish();
                        snackbar.dismiss();
                    }
                })
                .setActionTextColor(Color.WHITE);
        View snackView = snackbar.getView();
        snackView.setBackgroundColor(getResources().getColor(R.color.greenColor));
        TextView textView = snackView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }






}
