package com.example.employeeinfo;


import com.example.employeeinfo.PlaceAPI.Prediction;

public interface PredictionInterface {
    void getPrediction(Prediction prediction);
}
