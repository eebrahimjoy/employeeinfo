package com.example.employeeinfo;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.employeeinfo.databinding.ActivityMainBinding;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;
    private int flag = 0;
    private Snackbar snackbar;
    private static final String YOU_ARE_SUCCESSFULLY_LOGGED_IN = " Your are successfully logged in";
    private static final String INCORRECT_CREDENTIAL = " Your username or password incorrect";

    EditText username;
    EditText password;
    TextView responseText;
    Button logIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocale();
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        logIn=findViewById(R.id.loginBtnId);

        username = findViewById(R.id.userNameET);
        password = findViewById(R.id.passwordET);
        responseText = findViewById(R.id.resposeTV);

        binding.changeLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLanguageChangeDialog();
            }
        });




        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userNameText = username.getText().toString();
                String passwordText = password.getText().toString();

                if (userNameText.isEmpty() && passwordText.isEmpty()) {
                    Toast.makeText(MainActivity.this, "First fill-up the form", Toast.LENGTH_SHORT).show();
                } else if (userNameText.isEmpty() || passwordText.isEmpty()) {
                    if (userNameText.isEmpty()) {
                        Toast.makeText(MainActivity.this, "First enter the username", Toast.LENGTH_SHORT).show();
                    }
                    if (passwordText.isEmpty()) {
                        Toast.makeText(MainActivity.this, "First enter the password", Toast.LENGTH_SHORT).show();
                    }
                }
                if (!userNameText.isEmpty() && !passwordText.isEmpty()) {
                    if (userNameText.equals("ebrahim") && passwordText.equals("1234")){
                        gotoEmplyeeInfoActivity();
                        username.setText("");
                        password.setText("");
                    }
                    else {
                        showResponse();
                    }


                }

            }
        });
    }

    private void showLanguageChangeDialog() {
        final String[] langListIteam = {"English","বাংলা"};
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        mBuilder.setTitle("Select Language...");
        mBuilder.setSingleChoiceItems(langListIteam, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0){
                    setLocale("en");
                    recreate();
                }
                else if (i == 1){
                    setLocale("bn");
                    recreate();
                }
                dialogInterface.dismiss();
            }
        });
        AlertDialog mDialog = mBuilder.create();
        mDialog.show();
    }

    private void setLocale(String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences.Editor editor = getSharedPreferences("Settings",MODE_PRIVATE).edit();
        editor.putString("MY_LANG",language);
        editor.apply();

    }
    private void loadLocale(){
        SharedPreferences sharedPreferences = getSharedPreferences("Settings",MODE_PRIVATE);
        String language = sharedPreferences.getString("MY_LANG","");
        setLocale(language);
    }


    public void showResponse() {
        snackbar = Snackbar.make(responseText, "" +INCORRECT_CREDENTIAL, Snackbar.LENGTH_INDEFINITE)
                .setAction("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        snackbar.dismiss();
                    }
                })
                .setActionTextColor(Color.WHITE);
        View snackView = snackbar.getView();
        snackView.setBackgroundColor(getResources().getColor(R.color.greenColor));
        TextView textView = snackView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    public void gotoEmplyeeInfoActivity() {
        Intent i = new Intent(this, EmployeeInfoActivity.class);
        startActivity(i);
        finish();
    }


}
